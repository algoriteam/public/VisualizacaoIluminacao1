﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

public class Timer
{

    private Stopwatch timer;
    private TimeSpan elapsed;

    private string text;
    private bool used;

    public Timer()
    {
        this.timer = new Stopwatch();
        this.text = "";
        this.used = false;
    }

    public void start()
    {
        timer.Start();
    }

    public int stop()
    {
        timer.Stop();
        this.used = true;
        this.elapsed = timer.Elapsed;
        return this.elapsed.Milliseconds;
    }

    public void setText(string s)
    {
        this.text = s;
    }

    public string getText()
    {
        return this.text;
    }

    public int getValue()
    {
        return this.elapsed.Milliseconds;
    }

    public bool runned()
    {
        return this.used;
    }
}
