﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Main : MonoBehaviour {
    // City generation seed
    public string seed = "andre";

    // City plane size
    public int chunk_width = 64;
    public int chunk_length = 64;

    // Max building height
    public float max_building_height = 30.0f;
    public float residential_height_percentage = 0.1f;
    public float comercial_height_percentage = 0.4f;
    public float skyscraper_height_percentage = 1f;

    // Buildings length(X) and width(Y)
    public float max_residential_length = 2.0f;
    public float max_residential_width = 1.5f;

    public float max_comercial_length = 4.0f;
    public float max_comercial_width = 4.0f;

    public float max_skyscraper_length = 6.0f;
    public float max_skyscraper_width = 5.0f;

    // L-System number of generations
    public int road_generation_depth = 4;

    // City generation current fase
    private int generation_fase = 0;

    // Normal city
    private Map city;

    // Chunk city

        // Camera position
        private Vector3 cam_pos;

        // Camera grid position
        private int pos_x;
        private int pos_z;
        
        // City chunks (9 in use + garbage)
        private Dictionary<string, Map> chunk_city;
        private List<string> current_strings;
        private List<string> garbage_strings;

        // Preloaded chunks
        public int level_of_loaded_chunks = 1;
        public int level_of_preloaded_chunks = 1;

        // Garbage collector
        public int max_garbage_size = 50;
        private bool preload_ready;
        private bool garbage_ready;

    // Configs

        // Key configs
        private KeyCode key_enter = KeyCode.Return;
        private KeyCode key_space = KeyCode.Space;
        private KeyCode key_tab = KeyCode.Tab;
        private KeyCode key_n = KeyCode.N;
        private KeyCode key_r = KeyCode.R;
        private KeyCode key_p = KeyCode.P;
        private KeyCode key_g = KeyCode.G;

        private KeyCode key_f = KeyCode.F;
        private KeyCode key_c = KeyCode.C;

        // Timings
        private List<Timer> timings;

        // State
        private int state = 0;

    // GUI

    // Box content
        private bool pressed_tab;
        private GUIContent content;
        private GUIContent garbage;

        private GUIContent test1;
        private GUIContent test2;
        private GUIContent test3;

    // First person
        private bool person_mode;
        private GameObject person;

    // Use this for initialization
    void Start () {
        // Initiate timings lists
        createTimers(12);

        // 0 - Initiate city map
        this.city = new Map(
                // Starting point
                Vector3.zero,
                // City plane size
                this.chunk_width, this.chunk_length,
                // Buildings max height
                this.max_building_height,
                // Buildings residential category
                this.max_residential_length,
                this.max_residential_width,
                this.residential_height_percentage,
                // Buildings comercial category
                this.max_comercial_length,
                this.max_comercial_width,
                this.comercial_height_percentage,
                // Buildings skyscraper category
                this.max_skyscraper_length,
                this.max_skyscraper_width,
                this.skyscraper_height_percentage,
                // Road generation depth
                this.road_generation_depth,
                // City generation seed
                this.seed
            );

        // Infinite city
        this.pos_x = 0;
        this.pos_z = 0;
        this.chunk_city = new Dictionary<string, Map>();
        this.current_strings = new List<string>();
        this.garbage_strings = new List<string>();

        // GUI control
        this.content = new GUIContent("UCityGenerator");
        this.garbage = new GUIContent("0%");

        this.test1 = new GUIContent("X: " + this.cam_pos.x + " Z: " + this.cam_pos.z);
        this.test2 = new GUIContent("0 0 0\n0 0 0\n0 0 0");
        this.test3 = new GUIContent("X: " + this.pos_x + " Z: " + this.pos_z);

        this.preload_ready = true;
        this.garbage_ready = true;

        // Person
        this.person = Instantiate(Resources.Load("FirstPersonCharacter/Prefabs/FPSController", typeof(GameObject)), Vector3.zero, Quaternion.identity) as GameObject;
        this.person.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if ((this.state == 0 || this.state == 1) && Input.GetKeyDown(key_space) && this.generation_fase < 13)
        {
            this.state = 1;
            this.generation_fase++;

            switch (this.generation_fase)
            {
                case 1:
                    this.content.text = "[1] Creating road LSystem ...";

                    // 1 - Create road LSystem
                    setTimer(0, "    [1] - Create road LSystem");
                    startTimer(0);
                    this.city.createLSystem(true);
                    stopTimer(0);

                    this.content.text = "[1] Creating road LSystem ... Done in " + getTime(0) + " ms";
                    break;
                case 2:
                    this.content.text = "[2] Generating roads ...";

                    // 2 - Generate roads (Includes city limits)
                    setTimer(1, "    [2] - Generate roads");
                    startTimer(1);
                    this.city.generateRoads();
                    stopTimer(1);

                    this.content.text = "[2] Generating roads ... Done in " + getTime(1) + " ms";
                    break;
                case 3:
                    this.content.text = "[3] Drawing roads ...";

                    // 3 - Draw roads
                    setTimer(2, "    [3] - Draw roads");
                    startTimer(2);
                    StartCoroutine(this.city.drawRoadsRoutine());
                    stopTimer(2);

                    this.content.text = "[3] Drawing roads ... Done in " + getTime(2) + " ms";
                    break;
                case 4:
                    this.content.text = "[4] Extracting available lots to build on ...";

                    // 4 - Extract available lots to build on
                    setTimer(3, "    [4] - Extract available lots to build on");
                    startTimer(3);
                    this.city.generateLots(true);
                    stopTimer(3);

                    this.content.text = "[4] Extracting available lots to build on ... Done in " + getTime(3) + " ms";
                    break;
                case 5:
                    this.content.text = "[5] Creating height map ...";

                    // 5 - Create height map (Use perlin filter)
                    setTimer(4, "    [5] - Create height map (Use perlin filter)");
                    startTimer(4);
                    this.city.generateHeightMap(true);
                    stopTimer(4);

                    this.content.text = "[5] Creating height map ... Done in " + getTime(4) + " ms";
                    break;
                case 6:
                    this.content.text = "[6] Setting up each lot's category and stuff ...";

                    // 6 - Apply filter for each city lot (Assign category, type, calculate width, length and height)
                    setTimer(5, "    [6] - Apply filter and assign category for each city lot");
                    startTimer(5);
                    this.city.assignLots();
                    stopTimer(5);

                    this.content.text = "[6] Setting up each lot's category and stuff ... Done in " + getTime(5) + " ms";
                    break;
                case 7:
                    this.content.text = "[7] Drawing each lot's corners ...";

                    // 7 - Draw lot corner
                    setTimer(6, "    [7] - Draw each city lot corners");
                    startTimer(6);
                    StartCoroutine(this.city.drawLotCornersRoutine());
                    stopTimer(6);

                    this.content.text = "[7] Drawing each lot's corners ... Done in " + getTime(6) + " ms";
                    break;
                case 8:
                    this.content.text = "[8] Drawing each lot's category tile color ...";

                    // 8 - Draw lot category tile map
                    setTimer(7, "    [8] - Draw city lot category color tiles");
                    startTimer(7);
                    this.city.clearCorners();
                    StartCoroutine(this.city.drawLotTilesRoutine());
                    stopTimer(7);

                    this.content.text = "[8] Drawing each lot's category tile color ... Done in " + getTime(7) + " ms";
                    break;
                case 9:
                    this.content.text = "[9] Generating available building points for each lot ...";

                    // 9 - Generate available building points in each lot
                    setTimer(8, "    [9] - Generate available building points in each city lot");
                    startTimer(8);
                    this.city.undrawLotTiles();
                    this.city.generateBuildingPoints();
                    stopTimer(8);

                    this.content.text = "[9] Generating available building points for each lot ... Done in " + getTime(8) + " ms";
                    break;
                case 10:
                    this.content.text = "[10] Drawing all available building points ...";

                    // 10 - Draw available building points
                    setTimer(9, "    [10] - Draw all available building points");
                    startTimer(9);
                    StartCoroutine(this.city.drawBuildingPointsRoutine());
                    stopTimer(9);

                    this.content.text = "[10] Drawing all available building points ... Done in " + getTime(9) + " ms";
                    break;
                case 11:
                    this.content.text = "[11] Generating all buildings meshes ...";

                    // 11 - Generate buildings
                    setTimer(10, "    [11] - Generate all buildings");
                    startTimer(10);
                    this.city.clearBuildingPoints();
                    this.city.generateBuildings();
                    stopTimer(10);

                    this.content.text = "[11] Generating all buildings meshes ... Done in " + getTime(10) + " ms";
                    break;
                case 12:
                    this.content.text = "[12] Drawing all buildings ...";

                    // 12 - Draw buildings
                    setTimer(11, "    [12] - Draw all buildings");
                    startTimer(11);
                    StartCoroutine(this.city.drawBuildingsRoutine());
                    stopTimer(11);

                    this.content.text = "[12] Drawing all buildings ... Done in " + getTime(11) + " ms";
                    break;
                case 13:
                    this.content.text = "Saving results to Results_" + this.seed + ".txt";

                    // Save results
                    printTimers();
                    break;
            }
        }

        if (this.state == 0 && Input.GetKeyDown(key_n))
        {
            this.state = 2;

            this.content.text = "Generating city with seed '" + this.seed + "' ...";
            // 1 - Create road LSystem
            setTimer(0, "    [1] - Create road LSystem");
            startTimer(0);
            this.city.createLSystem(true);
            stopTimer(0);

            // 2 - Generate roads (Includes city limits)
            setTimer(1, "    [2] - Generate roads");
            startTimer(1);
            this.city.generateRoads();
            stopTimer(1);

            // 3 - Draw roads
            setTimer(2, "    [3] - Draw roads");
            startTimer(2);
            this.city.drawRoads();
            stopTimer(2);

            // 4 - Extract available lots to build on
            setTimer(3, "    [4] - Extract available lots to build on");
            startTimer(3);
            this.city.generateLots(true);
            stopTimer(3);

            // 5 - Create height map (Use perlin filter)
            setTimer(4, "    [5] - Create height map (Use perlin filter)");
            startTimer(4);
            this.city.generateHeightMap(true);
            stopTimer(4);

            // 6 - Apply filter for each city lot (Assign category, type, calculate width, length and height)
            setTimer(5, "    [6] - Apply filter and assign category for each city lot");
            startTimer(5);
            this.city.assignLots();
            stopTimer(5);

            // 7 - Draw lot corners
            //setTimer(6, "    [7] - Draw each city lot corners");
            //startTimer(6);
            //this.city.drawLotCorners();
            //stopTimer(6);

            // 8 - Draw lot category tile map
            //setTimer(7, "    [8] - Draw city lot category color tiles");
            //startTimer(7);
            //this.city.clearCorners();
            //this.city.drawLotTiles();
            //stopTimer(7);

            // 9 - Generate available building points in each lot
            setTimer(8, "    [9] - Generate available building points in each city lot");
            startTimer(8);
            this.city.undrawLotTiles();
            this.city.generateBuildingPoints();
            stopTimer(8);

            // 10 - Draw available building points
            //setTimer(9, "    [10] - Draw all available building points");
            //startTimer(9);
            //this.city1.drawBuildingPoints();
            //stopTimer(9);

            // 11 - Generate buildings
            setTimer(10, "    [11] - Generate all buildings");
            startTimer(10);
            this.city.clearBuildingPoints();
            this.city.generateBuildings();
            stopTimer(10);

            // 12 - Draw buildings
            setTimer(11, "    [12] - Draw all buildings");
            startTimer(11);
            this.city.drawBuildings();
            stopTimer(11);

            // Save results
            printTimers();

            this.content.text = "Generating city with seed '" + this.seed + "' ... Done in " + totalTimer() + " ms";
        }

        if (Input.GetKeyDown(key_r))
        {
            SceneManager.LoadScene("Testing");
        }

        if ((this.state == 0 && Input.GetKeyDown(key_enter)) || (this.state == 3 && this.preload_ready))
        {
            this.state = 3;
            check_chunks();
        }

        if ((this.state == 3 && Input.GetKeyDown(key_p)))
        {
            this.preload_ready = false;
            StartCoroutine(preload_chunks());
        }

        if ((this.state == 3 && Input.GetKeyDown(key_g)))
        {
            this.garbage_ready = false;
            StartCoroutine(clearGarbage(1));
        }

        if ((this.state == 3 && Input.GetKeyDown(key_f)))
        {
            person_mode = true;
            this.person.transform.position = transform.position;
            this.person.SetActive(true);
        }

        if ((this.state == 3 && Input.GetKeyDown(key_c)))
        {
            person_mode = false;
            transform.position = this.person.transform.position;
            transform.rotation = this.person.transform.rotation;
            this.person.SetActive(false);
        }

        if (Input.GetKeyDown(key_tab))
        {
            this.pressed_tab = !this.pressed_tab;
        }

        // Update garbage percentage
        StartCoroutine(updateGarbagePercentage());

    }

    // INFINITE CITY

    IEnumerator preload_chunks()
    {
        this.garbage_strings = garbage_positions(this.level_of_preloaded_chunks).Except(this.current_strings).ToList();
        float current = 0;
        float count = this.garbage_strings.Count;

        this.content.text = "Preparing " + count + " chunks to build city ... 0%";

        foreach(string s in this.garbage_strings)
        {
            List<int> pos = s.Split(',').Select(Int32.Parse).ToList();

            Map m = new Map(
                // Starting point
                new Vector3(pos[0] * this.chunk_width, 0.0f, pos[1] * this.chunk_length),
                // City plane size
                this.chunk_width, this.chunk_length,
                // Buildings max height
                this.max_building_height,
                // Buildings residential category
                this.max_residential_length,
                this.max_residential_width,
                this.residential_height_percentage,
                // Buildings comercial category
                this.max_comercial_length,
                this.max_comercial_width,
                this.comercial_height_percentage,
                // Buildings skyscraper category
                this.max_skyscraper_length,
                this.max_skyscraper_width,
                this.skyscraper_height_percentage,
                // Road generation depth
                this.road_generation_depth,
                // City generation seed
                (this.seed + pos[0] + pos[1])
            );

            this.chunk_city[s] = m;
            m.preloadCity();
            current++;

            this.content.text = "Preparing " + count + " chunks to build city ... " + Mathf.RoundToInt((current/count) * 100) + "%";
            yield return null;
        }

        this.content.text = "Warming up chunks ... 0%";

        current = 0;
        foreach (string c in this.garbage_strings)
        {
            this.chunk_city[c].redrawAll();
            this.chunk_city[c].undrawAll();
            current++;

            this.content.text = "Warming up chunks ... " + Mathf.RoundToInt((current / count) * 100) + "%";
            yield return null;
        }

        this.preload_ready = true;
        this.content.text = "UCityGenerator";
    }

    private void check_chunks()
    {
        update_camera_position();
        update_player_position();

        List<string> chunk_strings = neighbour_positions(this.level_of_loaded_chunks);
        List<string> sight_strings = neighbour_positions(1);

        // TESTING 2
        this.test2.text = "(" + sight_strings[0] + ") (" + sight_strings[1] + ") (" + sight_strings[2] + ")\n" +
                          "(" + sight_strings[7] + ") (" + this.pos_x + "," + this.pos_z + ") (" + sight_strings[3] + ")\n" +
                          "(" + sight_strings[6] + ") (" + sight_strings[5] + ") (" + sight_strings[4] + ")";

        // City chunks to undraw
        List<string> chunks_undraw = this.current_strings.Except(chunk_strings).ToList();
        foreach (string s in chunks_undraw)
        {
            // Add key to garbage and undraw city chunk
            this.current_strings.Remove(s);
            this.chunk_city[s].undrawAll();

            this.garbage_strings.Add(s);
        }

        // City chunks to add
        List<string> chunks_draw = chunk_strings.Except(this.current_strings).ToList();
        foreach (string s in chunks_draw)
        {
            this.current_strings.Add(s);
            Map m;

            if (this.garbage_strings.Contains(s))
            {
                this.garbage_strings.Remove(s);
                // Update garbage percentage
                int count = this.garbage_strings.Count;
                this.garbage.text = Mathf.RoundToInt((count / this.max_garbage_size) * 100) + "%";

                this.chunk_city[s].redrawAll();
            }
            else
            {
                List<int> pos = s.Split(',').Select(Int32.Parse).ToList();

                m = new Map(
                    // Starting point
                    new Vector3(pos[0] * this.chunk_width, 0.0f, pos[1] * this.chunk_length),
                    // City plane size
                    this.chunk_width, this.chunk_length,
                    // Buildings max height
                    this.max_building_height,
                    // Buildings residential category
                    this.max_residential_length,
                    this.max_residential_width,
                    this.residential_height_percentage,
                    // Buildings comercial category
                    this.max_comercial_length,
                    this.max_comercial_width,
                    this.comercial_height_percentage,
                    // Buildings skyscraper category
                    this.max_skyscraper_length,
                    this.max_skyscraper_width,
                    this.skyscraper_height_percentage,
                    // Road generation depth
                    this.road_generation_depth,
                    // City generation seed
                    (this.seed + pos[0] + pos[1])
                );

                this.chunk_city[s] = m;
                m.generateCity();
            }
        }

        // Check if garbage needs to be cleansed
        int garbage_size = this.garbage_strings.Count;
        if (garbage_size >= this.max_garbage_size)
        {
            StartCoroutine(clearGarbage(0.25f));
        }
    }

    private List<string> neighbour_positions(int i)
    {
        int start_i = this.pos_x - i;
        int end_i = this.pos_x + i;
        int start_j = this.pos_z - i;
        int end_j = this.pos_z + i;

        List<string> r = new List<string>();

        for (int a = start_i; a <= end_i; a++)
        {
            for(int b = start_j; b <= end_j; b++)
            {
                r.Add(a + "," + b);
            }
        }

        return r;
    }

    private List<string> garbage_positions(int i)
    {
        int start_i = this.pos_x - i;
        int end_i = this.pos_x + i;
        int start_j = this.pos_z - i;
        int end_j = this.pos_z + i;

        List<string> r = new List<string>();

        int quantity = 0;

        for (int a = start_i; a <= end_i; a++)
        {
            for (int b = start_j; b <= end_j; b++)
            {
                if (quantity < this.max_garbage_size)
                {
                    r.Add(a + "," + b);
                    quantity++;
                } else
                {
                    break;
                }
            }
        }

        return r;
    }

    private void update_player_position()
    {
        if(this.cam_pos.x > this.pos_x * this.chunk_width && this.cam_pos.x < this.chunk_width + this.pos_x * this.chunk_width &&
           this.cam_pos.z > this.pos_z * this.chunk_length && this.cam_pos.z < this.chunk_length + this.pos_z * this.chunk_length)
        {
            // Player didn't move in grid
        } else
        {
            if (this.cam_pos.x > this.chunk_width + this.pos_x * this.chunk_width) this.pos_x++;
            if (this.cam_pos.x < this.pos_x * this.chunk_width) this.pos_x--;
            if (this.cam_pos.z > this.chunk_length + this.pos_z * this.chunk_length) this.pos_z++;
            if (this.cam_pos.z < this.pos_z * this.chunk_length) this.pos_z--;
        }

        // TESTING 3
        this.test3.text = "X: " + this.pos_x + " Z: " + this.pos_z;
    }

    private void update_camera_position()
    {
        if (this.person_mode)
        {
            this.cam_pos = this.person.transform.position;
        }
        else
        {
            this.cam_pos = transform.position;
        }

        // TESTING 1
        this.test1.text = "X: " + this.cam_pos.x + " Z: " + this.cam_pos.z;
    }

    private IEnumerator updateGarbagePercentage()
    {
        float count = this.garbage_strings.Count;
        this.garbage.text = Mathf.RoundToInt((count / this.max_garbage_size) * 100) + "%";

        yield return null;
    }

    private IEnumerator clearGarbage(float percentage)
    {
        int count = this.garbage_strings.Count;
        int quad_size = Mathf.RoundToInt(count * percentage);

        this.content.text = "Cleaning up garbage ...";

        for (int i = 0; i < quad_size; i++)
        {
            System.Random rand = new System.Random();
            string randomKey = this.garbage_strings[rand.Next(count - 1)];

            // Remove random older key stored in garbage
            this.garbage_strings.Remove(randomKey);
            count--;

            if(this.chunk_city.ContainsKey(randomKey))
            {
                // Remove chunk city with chosen random key
                this.chunk_city[randomKey].clearAll();
                this.chunk_city.Remove(randomKey);
            }

            yield return null;
        }

        this.content.text = "UCityGenerator";

        this.garbage_ready = true;
    }

    // GUI

    private void OnGUI()
    {
        GUIStyle style1 = new GUIStyle(GUI.skin.box);
        style1.fontSize = 25;
        style1.normal.textColor = Color.white;
        style1.alignment = TextAnchor.MiddleCenter;

        GUIStyle style2 = new GUIStyle(GUI.skin.box);
        style2.fontSize = 25;
        style2.normal.textColor = Color.white;
        style2.alignment = TextAnchor.MiddleCenter;

        GUIStyle style3 = new GUIStyle();
        style3.fontSize = 20;
        style3.normal.textColor = Color.white;

        float count = this.garbage_strings.Count;
        int percentage = Mathf.RoundToInt((count / this.max_garbage_size) * 100);
        if (percentage < 50)
        {
            style2.normal.background = MakeTex(2, 2, new Color(0f, 1f, 0.02f, 0.5f));
        }
        else
        {
            if(percentage < 90)
            {
                style2.normal.background = MakeTex(2, 2, new Color(1f, 0.5f, 0.1f, 0.5f));
            }
            else
            {
                style2.normal.background = MakeTex(2, 2, new Color(1f, 0.3f, 0.3f, 0.5f));
            }
        }
        GUI.Box(new Rect(20, 20, Screen.width - 40, 50), this.content, style1);

        GUI.Box(new Rect(20, Screen.height - 70, 400, 50), this.test1, style1);
        GUI.Box(new Rect(Screen.width - 260, Screen.height - 120, 240, 100), this.test2, style1);
        GUI.Box(new Rect(Screen.width - 140, Screen.height - 190, 120, 50), this.test3, style1);

        GUI.Box(new Rect(20, Screen.height - 170, 100, 80), this.garbage, style2);

        if (this.pressed_tab)
        {
            GUI.Box(new Rect(20, 90, Screen.width - 40, Screen.height - 300), "", style1);
            GUI.Label(new Rect(40, 110, 200, 40), "HOTKEYS:", style3);

            GUI.Label(new Rect(60, 140, 200, 40), "Space - Live generation 1 chunk", style3);
            GUI.Label(new Rect(60, 170, 200, 40), "N - Generate 1 chunk", style3);
            GUI.Label(new Rect(60, 200, 200, 40), "Enter - Infinite city", style3);

            GUI.Label(new Rect(60, 230, 200, 40), "R - Reload scene", style3);
            GUI.Label(new Rect(60, 260, 200, 40), "P - Preload chunks", style3);
            GUI.Label(new Rect(60, 290, 200, 40), "G - Clean garbage", style3);

            GUI.Label(new Rect(440, 110, 200, 40), "CAMERAS:", style3);
            GUI.Label(new Rect(460, 140, 200, 40), "F - First person", style3);
            GUI.Label(new Rect(460, 170, 200, 40), "C - Free cam", style3);
        }
    }

    // EXTRAS

    private void createTimers(int n)
    {
        this.timings = new List<Timer>();
        for(int i = 0; i < n; i++)
        {
            this.timings.Add(new Timer());
        }
    }

    private void setTimer(int i, string s)
    {
        this.timings[i].setText(s);
    }

    private void startTimer(int i)
    {
        this.timings[i].start();
    }

    private void stopTimer(int i)
    {
        this.timings[i].stop();
    }

    private int getTime(int i)
    {
        return this.timings[i].getValue();
    }

    private int totalTimer()
    {
        int total = 0;

        foreach (Timer t in this.timings)
        {
            total += t.getValue();
        }

        return total;
    }

    private void printTimers()
    {
        TextWriter tw = new StreamWriter("Results_" + this.seed + ".txt");
        tw.WriteLine("------------------ RESULTS -----------------------------------------------");
        tw.WriteLine();
        foreach (Timer t in this.timings)
        {
            if (t.runned())
            {
                tw.WriteLine(t.getText());
                tw.WriteLine("    TIME: " + t.getValue() + " ms");
            } else
            {
                tw.WriteLine("    ------------------ EXTRA PHASE IGNORED ------------------");
            }
            tw.WriteLine();
        }
        tw.WriteLine("------------------ TOTAL -------------------------------------------------");
        tw.WriteLine("    TIME: " + totalTimer() + " ms");
        tw.WriteLine("--------------------------------------------------------------------------");
        tw.Flush();
    }

    private Texture2D MakeTex(int width, int height, Color col) {
        Color[] pix = new Color[width * height];

        for (int i = 0; i < pix.Length; i++)
        {
            pix[i] = col;
        }

        Texture2D result = new Texture2D(width, height);

        result.SetPixels(pix);
        result.Apply();

        return result;
    }

}
