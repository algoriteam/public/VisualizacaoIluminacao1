------------------ RESULTS -----------------------------------------------

    [1] - Create road LSystem
    TIME: 16 ms

    [2] - Generate roads
    TIME: 89 ms

    [3] - Draw roads
    TIME: 206 ms

    [4] - Extract available lots to build on
    TIME: 35 ms

    [5] - Create height map (Use perlin filter)
    TIME: 17 ms

    [6] - Apply filter and assign category for each city lot
    TIME: 17 ms

    ------------------ EXTRA PHASE IGNORED ------------------

    ------------------ EXTRA PHASE IGNORED ------------------

    [9] - Generate available building points in each city lot
    TIME: 45 ms

    ------------------ EXTRA PHASE IGNORED ------------------

    [11] - Generate all buildings
    TIME: 34 ms

    [12] - Draw all buildings
    TIME: 62 ms

------------------ TOTAL -------------------------------------------------
    TIME: 521 ms
--------------------------------------------------------------------------
